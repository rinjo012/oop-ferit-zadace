#pragma once
class Episode
{
private:
	int mViewers;
	float mSumOfScores,mMaxGrade;
public:
	Episode();
	Episode(int, float, float);
	~Episode();
	void addView(float);
	float getMaxScore() const;
	float getAverageScore() const;
	int getViewerCount() const;
};
float generateRandomScore();

