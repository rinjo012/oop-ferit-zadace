#include "Episode.h"
#include <iostream>
#include <cstdlib>

Episode::Episode(): mViewers(0), mMaxGrade(0), mSumOfScores(0)
{
}
Episode::Episode(int Viewers, float MaxGrade, float SumOfScores) : mViewers(Viewers), mMaxGrade(MaxGrade), mSumOfScores(SumOfScores) {
}

void Episode::addView(float Score) {
	if (mMaxGrade < Score)
		mMaxGrade = Score;
	mSumOfScores += Score;
	mViewers++;
}

float Episode::getMaxScore() const {
	return mMaxGrade;
}

float Episode::getAverageScore() const {
	return mSumOfScores / mViewers;
}

int Episode::getViewerCount() const {
	return mViewers;
}

Episode::~Episode(){}

float generateRandomScore() {
	return ((float)rand() / RAND_MAX) * 10;
}